class Queue:
    def __init__(self):
        self.elements = []

    def is_empty(self):
        return self.elements == []

    def enqueue(self, val):
        self.elements.insert(0, val)

    def dequeue(self):
        self.elements.pop(0)

    def first(self):
        return self.elements[0]

    def size(self):
        return len(self.elements)
