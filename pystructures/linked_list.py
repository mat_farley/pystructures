from __future__ import annotations
from typing import Optional, Type, TypeVar, Generic
from typing_extensions import TypeAlias

T = TypeVar('T')
S = TypeVar('S')
Node: TypeAlias = 'LinkedList.Node'

class LinkedList(Generic[T]):
    class Node(Generic[S]):
        def __init__(self, data: S, next_node: Node = None, prev_node: Node = None) -> None:
            self.data = data
            self._next = next_node
            self._prev = prev_node
    
    class ListerIterator:
        def __init__(self):
            pass

    def __init__(self, head: Node = None, tail: Node = None) -> None:
        self._head = head
        self._tail = tail
        self._size = 0

    def prepend(self, data: T) -> None:
        node = self.Node(data)
        if self._head:
            node._next = self._head
        if not self._tail:
            self._tail = node
        self._head = node
        self._size += 1

    def append(self, data: T) -> None:
        node = self.Node(data)
        if self._tail:
            node._prev = self._tail
        if not self._head:
            self._head = node
        self._tail = node
        self._size += 1

    def insert(self, i, data: T) -> None:
        if i > self._size:
            raise Exception
        if i == 0:
            self.prepend(data)
        elif i == self._size:
            self.append(data)
        else:
            current: Node = self._head  # type: ignore
            
            for _ in range(i-1):
                current = current._next # type: ignore
            
            node = self.Node(data)
            node._next = current._next
            node._prev = current
            current._next._prev = node # type: ignore
            current._next = node

    def size(self) -> int:
        return self._size

if __name__ == '__main__':
    ll = LinkedList.Node(1)
    print('hello')

