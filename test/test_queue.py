import unittest
from pystructures.queue import Queue

class Test(unittest.TestCase):

    def test_queue(self):
        queue = Queue()
        self.assertTrue(queue.is_empty())
        queue.enqueue(1)
        self.assertEqual(queue.first(), 1)
        self.assertFalse(queue.is_empty())
        self.assertEqual(queue.size(), 1)
        queue.dequeue()
        self.assertEqual(queue.size(), 0)

if __name__ == "__main__":
    unittest.main()

