import unittest
from pystructures.stack import Stack

class Test(unittest.TestCase):
    
    def test_stack(self):
        stack = Stack()
        self.assertTrue(stack.is_empty())
        stack.push(1)
        self.assertEqual(stack.peek(), 1)
        self.assertFalse(stack.is_empty())

if __name__ == "__main__":
    unittest.main()
