main: test clean

test:
	@python -m unittest

clean:
	@rm -rf test/__pycache__ pystructures/__pycache__

.PHONY: test
